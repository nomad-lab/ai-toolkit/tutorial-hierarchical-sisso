0             0.693840736207857356    ((B_r_s_1 * A_cAcB) / (B_IP + A_EN))
1             0.693712519214626733    ((B_r_s_1 / A_cB) / (B_IP + A_EN))
2             0.688727674874737184    ((B_r_s_1 * A_cAcB) / (B_IP + A_IP))
3             0.68864927483986782     ((A_cB / B_r_s_1) * (B_IP + A_EN))
4             0.68609319232783883     ((B_r_s / A_cB) / (B_IP + A_EN))
5             0.685262102183314958    ((B_r_s * A_cAcB) / (B_IP + A_EN))
6             0.679421926032011347    ((B_r_s_1 / A_cB) / (B_IP + A_IP))
7             0.678455443763880561    ((A_cB / B_r_s_1) / (B_EA - B_eps_L))
8             0.678143916403486702    ((B_r_s * A_cAcB) / (B_IP + A_IP))
9             0.676126264486393569    ((B_r_s_1 * A_cAcB) / (B_EN + A_EN))
10            0.671917707809127407    ((A_cB / B_r_s_1) * (B_IP + A_IP))
11            0.670204560515046843    ((B_r_s / A_cB) / (B_IP + A_IP))
12            0.668084876295865326    ((B_r_s_1 / A_cB) / (B_IP + A_EA))
13            0.665421342954702566    ((B_r_s / A_cB) / (B_IP + A_EA))
14            0.665166757061285363    ((B_r_s_1 / B_IP) * (B_r_s_1 + A_r_s_1))
15            0.664183185508365637    ((B_r_s_1 * A_cAcB) / (B_EN + A_IP))
16            0.664169635123126434    ((A_cB / B_r_s) * (B_IP + A_EN))
17            0.663558309148857384    ((B_r_s + A_r_s_1) * (B_r_s_1 / B_IP))
18            0.663524698177704986    ((B_EA - B_eps_L) * (B_r_s_1 / A_cB))
19            0.660757176441361893    ((B_r_s / B_IP) * (B_r_s_1 + A_r_s_1))
20            0.660705967047832132    ((A_cB / B_r_s) / (B_EA - B_eps_L))
21            0.659626824236671405    ((B_r_s / A_cB) * (B_EA - B_eps_L))
22            0.659265061636120531    ((B_r_s_1 + B_r_s) / (B_IP * A_cB))
23            0.659167876010706477    (B_r_s / (B_IP * A_cB))
24            0.658875400502646036    ((B_r_s_1^2) / (B_IP * A_cB))
25            0.658563373882126268    ((B_r_s * A_cAcB) / (B_EN + A_EN))
26            0.658201182339473778    (cbrt(A_cA) * (B_r_s_1 / B_IP))
27            0.657611042726480632    (cbrt(A_cA) * (B_r_s / B_IP))
28            0.65747717465320088     (B_r_s_1 / (B_IP * A_cB))
29            0.656498323276885176    ((B_r_s + A_r_s_1) * (B_r_s / B_IP))
30            0.655930499112369336    ((B_r_s_1 * A_cAcB) / (B_IP - A_eps_H))
31            0.655844972452989339    ((A_cB / B_r_s_1) * (B_IP - A_eps_H))
32            0.653983294205891297    ((B_r_s_1 * B_r_s) / (B_IP * A_cB))
33            0.653615569626762816    ((B_r_s_1 / B_IP) / sqrt(A_cB))
34            0.653359551164812702    ((A_cB / B_r_s_1) * (B_IP + A_EA))
35            0.653138944131976262    ((B_r_s * A_cAcB) / (B_IP - A_eps_H))
36            0.652001515656893571    ((B_r_s_1 * A_cAcB) / (|B_eps_H - A_IP|))
37            0.651536772396613784    ((B_r_s / B_IP) / sqrt(A_cB))
38            0.650642952398978736    ((B_r_s_1 / A_cB) / sqrt(B_IP))
39            0.650051222537859741    ((B_EN + A_EN) / (B_r_s_1 * A_cAcB))
40            0.648666396225570208    ((B_r_s / A_cB) / (B_EN + B_IP))
41            0.648102494792155426    ((B_r_s_1 / A_cB) / (B_EN + B_IP))
42            0.647852978967951243    ((A_cB / B_r_s) * (B_IP + A_IP))
43            0.647364653314834393    ((B_r_s * A_cAcB) / (B_EN + A_IP))
44            0.647146817919517225    ((B_r_s_1 * A_cAcB) / (|B_eps_H - A_EN|))
45            0.647109477833492375    ((B_IP + A_EN) / (B_r_s_1 * A_cAcB))
46            0.646036080518976985    ((B_IP + A_IP) / (B_r_s_1 * A_cAcB))
47            0.64572633791785361     ((B_r_s^2) / (B_IP * A_cB))
48            0.645060595176687346    ((A_cB / B_r_s_1) * B_IP)
49            0.644481795218976083    ((B_r_s_1 * A_cAcB) / (B_IP + A_EA))
#-----------------------------------------------------------------------
50            0.326261340060072236    ((B_IP / A_r_s) * (B_IP - B_EA))
51            0.322456007759246766    ((B_IP^3) / (B_EN * A_r_s))
52            0.318682444925918296    ((B_IP / A_r_s) * (B_IP - B_eps_L))
53            0.316407073715140486    ((B_IP / B_EN) / (B_EA - B_eps_L))
54            0.31522043614740225     ((B_IP - B_EA) / sqrt(A_r_s))
55            0.310416111369994807    ((A_EN / A_cAcB) * (B_IP - B_EA))
56            0.308062262868358361    ((A_r_s_1 + A_r_s) / (B_IP + A_EA))
57            0.305303543962401058    ((|B_EA - A_IP|) * (B_IP^2))
58            0.304717465220123718    ((B_Z^2) / (B_r_s_1 - B_r_val))
59            0.303027141623420593    ((B_IP / A_r_s) * (B_EN - B_eps_L))
60            0.302909070841793082    ((B_EN - A_IP) - (B_IP - A_EN))
61            0.301483849697653683    ((B_EA + B_eps_H) * (B_IP / A_r_s))
62            0.301300543038798108    ((B_IP - B_EA) / cbrt(A_r_s))
63            0.301243513803560459    ((B_IP + A_IP) * (B_IP - B_eps_L))
64            0.300864190350876581    ((|B_eps_L - A_IP|) * (B_IP^2))
65            0.3004151820494953      (|(B_EA / B_r_s_1) - (B_IP / A_r_s)|)
66            0.299999425162246924    ((B_IP + A_IP) * (B_IP - B_EA))
67            0.299799942712622947    ((B_EA / B_r_s) - (B_IP / A_r_s))
68            0.299674428489102362    ((B_EA + B_eps_H) * (B_EN / A_r_s))
69            0.299128341885084859    ((B_IP + A_IP) + (B_IP - B_eps_L))
70            0.299005851134597656    ((B_IP - A_eps_H) * (B_IP - B_eps_L))
71            0.298615720266241835    ((B_IP + A_EA) / (A_r_s_1 + A_r_s))
72            0.298098366332885889    ((|B_EA - A_IP|) * (B_IP^3))
73            0.2980255837377348      ((B_eps_L + B_eps_H) * (B_IP / A_r_s))
74            0.297341478576451435    ((B_IP - B_EA) / (B_r_s_1 + A_r_s))
75            0.29680490185962366     ((B_IP * A_IP) * (B_IP - B_EA))
76            0.296732103622738164    (|(B_EA / A_r_s) - (B_IP / A_r_s)|)
77            0.296628940378065586    ((B_IP + A_IP) + (B_IP - B_EA))
78            0.296017420596472125    ((B_IP^2) / (B_EN * A_r_s))
79            0.295934434192370643    ((B_EA - B_eps_L) * (B_EN / B_IP))
80            0.295508404613945119    ((B_EN + A_IP) * (B_IP - B_EA))
81            0.2948858480196756      ((B_IP - A_eps_H) * (B_IP - B_EA))
82            0.294486344106840658    ((A_EN - A_EA) + (B_IP - B_EA))
83            0.29436727938834617     ((A_IP / B_EN) * (B_IP^3))
84            0.294359282306873471    ((B_IP - B_eps_L) / sqrt(A_r_s))
85            0.293909264679424242    ((B_EA - B_eps_L) * (B_EN / B_eps_H))
86            0.293595001762627095    ((B_Z / A_cAcB) / (B_r_s_1 - B_r_val))
87            0.293259300512233423    ((B_IP - A_eps_H) + (B_IP - B_eps_L))
88            0.292869721930837823    ((A_cAcB * A_cB) * (B_EA - B_eps_L))
89            0.291932388114263863    ((B_IP - A_eps_H) + (B_IP - B_EA))
90            0.290688199510926115    ((B_IP - B_EA) * (B_eps_H / A_r_s))
91            0.290184433045077961    ((B_IP - B_eps_L) / cbrt(A_r_s))
92            0.289167967364990175    ((B_Z / A_cA) / (B_r_s_1 - B_r_val))
93            0.289161577853088492    ((B_Z * A_cB) / (B_r_s_1 - B_r_val))
94            0.289049208729862672    ((|B_EA - A_IP|) + (|B_IP - A_IP|))
95            0.288993370898053881    ((B_IP - B_eps_L) / (B_r_s_1 + A_r_s))
96            0.288949555227977994    ((B_r_val_1^6) / (B_r_s_1 - B_r_val))
97            0.288796329587353462    ((B_IP - B_EA) * (|B_eps_H - A_IP|))
98            0.288574011755949211    ((B_EN - A_eps_H) + (B_IP - B_EA))
99            0.288460755817954051    ((B_IP + A_eps_L) / (A_r_s_1 + A_r_s))
#-----------------------------------------------------------------------
100           0.244098106311383373    ((A_eps_H / B_r_s_1) - (A_eps_H / B_r_s))
101           0.242860212266327757    ((B_eps_L^2) + (B_IP * B_EA))
102           0.239332003114734126    ((B_EA + A_eps_H) * (B_r_s_1 - B_r_s))
103           0.230291754180303399    ((A_eps_H / B_r_s_1) * (B_r_s_1 - B_r_s))
104           0.22879566231528331     ((B_EN - A_eps_H) / (B_EA - B_eps_H))
105           0.227711915549797722    ((B_eps_L * A_eps_L) + (B_EN * B_EA))
106           0.227299309958703843    ((B_EA + A_eps_H) * (B_IP + B_eps_L))
107           0.226170763995696272    ((A_eps_H / B_r_s) * (B_r_s_1 - B_r_s))
108           0.220736602766613527    ((B_EA - B_eps_L) * (B_EN / B_r_s))
109           0.220161495881643243    ((B_IP * A_eps_H) * (B_r_s_1 - B_r_s))
110           0.21837792885882687     ((B_r_s * A_eps_H) / (B_EA - B_eps_L))
111           0.218296365267505649    ((B_eps_L + A_eps_H) / (B_EA - B_eps_L))
112           0.216901451505021103    (|(B_EA - B_eps_L) - (B_eps_L - B_eps_H)|)
113           0.21639681180872497     ((B_EA + A_eps_H) + (B_EA - B_eps_L))
114           0.215529553480933639    ((B_EA - A_eps_L) + (B_EA - B_eps_L))
115           0.213743131486039739    ((B_IP - A_eps_H) / (B_EA - B_eps_H))
116           0.212988552572391299    ((A_EN * A_eps_H) * (B_r_s_1 - B_r_s))
117           0.211224423515181586    ((A_IP * A_eps_H) * (B_r_s_1 - B_r_s))
118           0.210556636652209778    ((A_eps_H^2) * (B_r_s_1 - B_r_s))
119           0.209366963217360397    ((B_IP + A_eps_L) / (B_EA - B_eps_H))
120           0.208627633256065148    ((B_IP * A_eps_H) / (B_EA - B_eps_H))
121           0.206885030399374337    ((B_r_s_1 - B_r_s)^3)
122           0.206021042725211628    ((A_eps_H / A_r_s) * (B_r_s_1 - B_r_s))
123           0.205384991567902075    ((B_EA + A_eps_H) * (B_IP + B_eps_H))
124           0.205250434618477168    ((|B_EA - A_eps_L|) + (B_EA - B_eps_L))
125           0.20494141821842693     ((A_r_val_1 + A_r_s_1) * (B_EA - B_eps_L))
126           0.204622840937687678    ((B_EA - B_eps_L) * (B_EN / B_r_s_1))
127           0.203807928272108807    ((B_r_s * A_eps_H) / (B_EA - B_eps_H))
128           0.20370351065406625     ((B_r_s_1 * A_eps_H) / (B_EA - B_eps_L))
129           0.202984147665464437    ((B_r_s / A_r_s) / (B_EA - B_eps_L))
130           0.200960571630174223    ((B_EA + A_eps_H) * (B_IP / B_eps_H))
131           0.200945460677892179    ((A_eps_H / B_eps_H) * (B_r_s_1 - B_r_s))
132           0.200902896848307977    ((B_EA / A_eps_L) + (B_eps_L / B_EN))
133           0.200637261065577305    ((B_EA - A_EN) * (B_r_s_1 - B_r_s))
134           0.200217329827757473    ((|A_eps_H|) * (B_r_s_1 - B_r_s))
135           0.200195745522456087    ((A_cB * A_r_s_1) * (B_EA - B_eps_L))
136           0.200190234772426201    ((B_eps_L * B_eps_H) + (B_IP * B_EA))
137           0.19974021653494714     ((B_EN + A_eps_H) - (B_IP + B_eps_H))
138           0.199734019585133815    (|(B_EA + A_eps_H) - (B_EN + B_eps_H)|)
139           0.199409324933983723    ((B_IP + B_eps_H) / (B_EN - A_eps_L))
140           0.199270422962365884    ((A_eps_L - A_eps_H) - (B_EA - B_eps_L))
141           0.198893654150732069    ((B_EA - B_eps_L) + (B_EN + A_eps_H))
142           0.198431692606426874    ((B_EN * A_eps_H) / (B_EA - B_eps_H))
143           0.19802047920707172     ((B_EA + A_eps_H) / (B_eps_L + B_eps_H))
144           0.197693989494418465    ((B_EA * B_eps_H) - (B_eps_L * A_eps_L))
145           0.197561325965171258    ((B_eps_L - B_eps_H) / (B_EA^3))
146           0.19703592312396373     ((B_EA * A_IP) + (B_eps_L * A_eps_L))
147           0.197014605175098084    ((B_IP - B_EA) / (B_eps_H + A_eps_L))
148           0.196755548864320645    ((B_r_s / B_eps_H) * (B_EA + A_eps_H))
149           0.196151412450229462    ((B_EA + A_eps_H) / (B_eps_L + A_eps_L))
#-----------------------------------------------------------------------
150           0.19569951781360892     ((B_IP * A_EN) / (B_r_val + A_r_s_1))
151           0.195683741747059603    ((B_IP * A_EN) / (B_r_val_1 + A_r_s_1))
152           0.188384401761034187    ((B_EN * A_EN) / (B_r_val + A_r_s_1))
153           0.18769683937670445     ((B_EN * A_EN) / (B_r_val_1 + A_r_s_1))
154           0.180740793204838462    ((A_r_s_1 / B_r_s) + (A_EA / A_eps_L))
155           0.175624489911576115    (A_EN / (B_r_val_1 + A_r_s_1))
156           0.175496261535102616    ((A_IP + A_EA) / (B_r_val + A_r_s_1))
157           0.17522276223401495     ((A_r_s_1 / B_r_s_1) + (A_EA / A_eps_L))
158           0.174175455933382767    ((B_EA - A_eps_L) * (B_eps_L^6))
159           0.173860737659059711    ((|B_r_s - A_r_val|) / (B_eps_H^2))
160           0.172257726849403103    ((|B_r_s - A_r_val|) / (B_eps_H^3))
161           0.171362888651725748    ((|B_r_s_1 - A_r_val|) / (B_eps_H^2))
162           0.170371173387473152    ((A_eps_L / A_EN) * (B_r_val_1 + A_r_s_1))
163           0.169983343973614892    ((A_EN + A_EA) / (B_r_val_1 + A_r_s_1))
164           0.169894395473850851    ((A_EN + A_EA) / (B_r_val + A_r_s_1))
165           0.168940203198401973    ((|B_r_s_1 - A_r_val|) / (B_eps_H^3))
166           0.168327075525517678    ((A_eps_L / A_EN) * (B_r_val + A_r_s_1))
167           0.167344660045188082    ((B_r_s * A_EN) + (A_eps_L * A_r_s_1))
168           0.166988521744457996    ((B_eps_H * A_EN) / (B_r_val_1 + A_r_s_1))
169           0.166381271528820651    ((B_eps_H * A_EN) / (B_r_val + A_r_s_1))
170           0.165963577383462596    ((|B_r_val_1 - B_r_s_1|) / (B_eps_L^6))
171           0.165269586074546448    ((|B_r_s_1 - A_r_val|) / (B_eps_H + A_EA))
172           0.165089481940955796    ((|B_r_s_1 - A_r_val|) / (B_eps_L + B_eps_H))
173           0.164957500783052824    ((A_EA - A_eps_H) / (B_r_val + A_r_s_1))
174           0.164745327697573718    ((A_r_s_1 / A_EN) + (B_r_val_1 / A_EN))
175           0.16458106133298786     ((A_EA - A_eps_H) / (B_r_val_1 + A_r_s_1))
176           0.16383123890928547     ((|B_r_s - A_r_val|) / (B_eps_L + B_eps_H))
177           0.163537359086294992    ((A_IP + A_eps_L) / (B_r_val + A_r_s_1))
178           0.163065039297921627    (|(A_r_s_1 / A_EN) - (A_r_s / A_IP)|)
179           0.163024661118174541    ((|B_r_s_1 - A_r_val|) / (|B_eps_H|))
180           0.16294127579655393     ((A_IP + A_eps_L) / (B_r_val_1 + A_r_s_1))
181           0.162581278254050859    ((|B_r_s - A_r_val|) / (|B_eps_H|))
182           0.162132639735261597    ((|B_r_s_1 - A_r_val|) / (B_eps_H * A_r_s))
183           0.16209844263698181     ((B_EN / B_eps_H) * (|B_r_s_1 - A_r_val|))
184           0.161949884176482845    ((B_IP / B_eps_H) * (|B_r_s_1 - A_r_val|))
185           0.161768122428597005    ((A_r_s_1 / B_r_val) / (B_EN + B_eps_H))
186           0.161732085024611921    ((B_r_val_1 - A_r_s_1) / (B_EN + B_eps_H))
187           0.161704722813624802    ((A_eps_L * A_r_s_1) + (B_r_s_1 * A_EN))
188           0.161664834662566398    ((B_IP * A_IP) / (B_r_val + A_r_s_1))
189           0.1615500173264586      ((A_r_s_1 / B_r_val_1) / (B_EN + B_eps_H))
190           0.161532472248296838    ((|B_r_s - A_r_val|) * (B_EN / B_eps_H))
191           0.160831094698992899    ((A_IP / B_eps_H) + (A_r_s_1 / A_r_s))
192           0.160806229689369112    ((B_IP * A_IP) / (B_r_val_1 + A_r_s_1))
193           0.160219565160132332    ((|B_r_s - A_r_val|) * (B_IP / B_eps_H))
194           0.160173636130016617    ((B_r_val + A_r_s_1) / A_EN)
195           0.160062707171044982    ((B_r_s * A_EN) / (B_r_s_1 + A_r_s_1))
196           0.160016062056750952    ((B_r_val - A_r_s_1) / (B_EN + B_eps_H))
197           0.159577844494498911    ((A_cB / B_eps_H) / (B_EA - B_eps_L))
198           0.159006978578085878    ((A_IP / B_eps_H) + (A_r_val_1 / A_r_val))
199           0.158984423782579348    ((A_EN / B_eps_H) + (A_r_s_1 / A_r_s))
#-----------------------------------------------------------------------
200           0.154555706333755843    ((B_EA^6) * (B_EA * A_cB))
201           0.152227946104134843    ((B_EA^6) * (B_EA / A_cAcB))
202           0.151489971841877152    ((B_EA^6) * (B_EN * B_EA))
203           0.150905817350110216    ((B_EA^3)^3)
204           0.150418173517791853    ((B_EA^6) * (B_EA * B_eps_H))
205           0.15009868479865629     ((B_EA^6) * (B_IP * B_EA))
206           0.149062145207548125    ((B_EA^6) * (B_EA / B_r_val_1))
207           0.149023499870926973    ((B_EA^6) * (B_EA * B_r_s))
208           0.149020643659826535    ((B_EA^6) * (B_EA / B_r_val))
209           0.148861063887241851    ((B_EA^6) * (B_EA * B_r_s_1))
210           0.148696898847826053    ((B_EA^6) * (B_EN + B_eps_L))
211           0.148133077179741141    ((B_EA^6) * (B_EA * B_Z))
212           0.147806201024867218    ((B_EA^6) * (B_EA / B_r_s_1))
213           0.147761407765794467    ((B_EA^6) * (B_r_s_1 - B_r_val))
214           0.147520488328390736    ((B_EA^6)^2)
215           0.14748862446585756     ((B_EA^6) * (B_EA / B_r_s))
216           0.146817474212507126    ((B_EA / A_r_s) * (B_EA^6))
217           0.146275805263412384    ((B_EA^6) * (B_EA / B_eps_H))
218           0.146204535972797589    ((B_EA^6) * (B_r_val - B_r_s))
219           0.146028016594684867    ((B_EA^6) * (B_r_val_1 - B_r_s_1))
220           0.145914804533911008    ((B_EA^6) * (B_EA / B_Z))
221           0.14556075399877022     ((B_EA^6) * (B_EA / A_IP))
222           0.145233804022119078    ((B_EA^6) * (B_EA / A_EN))
223           0.145199241049520894    ((B_EA^6) * (B_EA * B_r_val_1))
224           0.1450741518936777      ((B_EA^6) * (B_eps_L - A_eps_L))
225           0.144842819616049168    ((B_EA^6) * (B_EA * B_r_val))
226           0.144591998103549085    ((B_EA^6) * (B_r_val_1 - B_r_s))
227           0.144521633945631112    ((B_EA^6) / (|B_r_val_1 - B_r_val|))
228           0.144388283513921606    ((B_EA^6) * (B_EA * B_eps_L))
229           0.14426340821943337     ((B_EA^6) * (B_eps_L * A_cB))
230           0.142936402042748462    ((B_EA^6) * (B_EA / A_cA))
231           0.142631865154927817    ((B_EA^6) * (B_Z * A_cB))
232           0.142513487612616147    (|(A_EA^6) - (B_EA^6)|)
233           0.142464395088324036    ((B_EA^6) * (B_eps_L^3))
234           0.142428247150848897    ((B_EA^6) / (B_r_val_1 - B_r_val))
235           0.142338987232339592    ((B_EA^6) * (B_eps_L * B_Z))
236           0.142288812092316141    ((B_EA^6) * (B_EA * A_IP))
237           0.14210253853529331     ((B_EA^6) * (B_eps_L / A_cAcB))
238           0.142043311088594887    ((B_EA / B_Z) / (B_EN + B_eps_L))
239           0.141938270479385065    ((B_EA / A_eps_L) * (B_EA^6))
240           0.141698184205249267    ((A_cB / B_r_val) * (B_EA^6))
241           0.141617465483199478    ((B_EA^6) * (B_eps_L / B_r_val))
242           0.141432211745223857    ((B_EA^6) * (B_eps_L / B_r_val_1))
243           0.141244469494553493    ((B_EA^6) * (B_Z / B_r_val))
244           0.141131731080629541    ((A_cB / B_r_val_1) * (B_EA^6))
245           0.141120930839409769    ((B_EA^6) * (B_EA + B_eps_L))
246           0.140983947712480934    ((B_EA^6) * (B_Z / B_r_val_1))
247           0.140749760831490989    ((B_EA^6) * (B_eps_L^2))
248           0.140547923016215742    ((B_EA^6) * (B_Z / A_cAcB))
249           0.140546099852399881    ((B_EA^6) * (B_eps_L + A_EA))
#-----------------------------------------------------------------------
