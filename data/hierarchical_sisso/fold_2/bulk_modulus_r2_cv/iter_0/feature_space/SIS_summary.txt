0             0.624900520306886698    ((B_IP * A_cB) * (B_r_val_1 / B_r_s_1))
1             0.622388117831161103    ((B_IP * A_cB) * (B_r_val_1 / B_r_s))
2             0.619569032873203307    ((B_r_val + A_r_val) * (B_IP * B_r_val_1))
3             0.619378101641897216    ((B_IP * B_r_val_1) * (B_r_val_1 + A_r_val))
4             0.617486888156423941    ((B_IP / B_r_s_1) * (B_r_val * A_cB))
5             0.616718495271444733    ((B_r_val + A_r_val) * (B_eps_H * B_r_val))
6             0.616026601754598779    ((B_IP * B_r_val) * (B_r_val_1 + A_r_val))
7             0.615347047813667558    ((B_IP * A_cB) * (B_r_val / B_r_s))
8             0.615345719187333606    ((B_r_val + A_r_val) * (B_eps_H * B_r_val_1))
9             0.614873357210077409    ((B_r_val + A_r_val) * (B_IP * B_r_val))
10            0.614868104028754359    ((B_r_val_1 + A_r_val) * (B_eps_H * B_r_val))
11            0.613636867215145543    ((B_eps_H * A_cB) * (B_r_val_1 / B_r_s_1))
12            0.613374129108597343    ((B_eps_H / B_r_s_1) * (B_r_val * A_cB))
13            0.612423440864561908    ((B_eps_H * B_r_val_1) * (B_r_val_1 + A_r_val))
14            0.612123982092634078    ((B_IP * B_r_val_1) / (B_r_s_1 * A_cAcB))
15            0.612108309372127457    ((B_eps_H * A_cB) * (B_r_val_1 / B_r_s))
16            0.611994089248408812    ((B_r_val * A_cB) * (B_eps_H / B_r_s))
17            0.610738177866582022    ((B_IP * B_r_val_1) / (B_r_s * A_cAcB))
18            0.610623408489138542    ((B_IP * B_r_val) / (A_IP + A_eps_H))
19            0.609875657334311483    ((B_IP * B_r_val_1) / (A_IP + A_eps_H))
20            0.609116336185047791    (|(A_eps_L * A_r_val) - (B_IP * B_r_val_1)|)
21            0.608795217444162873    (|(A_eps_L * A_r_val) - (B_IP * B_r_val)|)
22            0.607245055810843426    ((B_IP * A_cB) * (B_r_val_1 + B_r_val))
23            0.607009305870883198    ((B_IP * B_r_val) / (B_r_s_1 * A_cAcB))
24            0.606866060986857359    ((B_IP - B_eps_H) * (B_r_val * A_cB))
25            0.606862044989473604    ((A_cAcB / A_cA) * (B_IP * B_r_val_1))
26            0.606823414158265018    ((B_IP * B_r_val) * A_cB)
27            0.605908150997565342    ((B_r_val_1^3) * (B_eps_H * A_cB))
28            0.605818258780755015    ((B_IP * B_r_val) / (B_r_s * A_cAcB))
29            0.605065776816620993    ((B_r_val_1^3) * (B_eps_H / A_cAcB))
30            0.604292034916559362    ((B_r_val_1^2) * (B_eps_H * A_cB))
31            0.604268550727103615    ((B_IP - B_eps_H) * (B_r_val_1 * A_cB))
32            0.603764863141003194    ((B_eps_H * B_r_val) / (B_r_s_1 * A_cAcB))
33            0.603398138483161595    ((B_eps_H * B_r_val) * (B_r_val_1 * A_cB))
34            0.603016441493657629    ((B_eps_H * B_r_val) / (B_r_s * A_cAcB))
35            0.60273384702524524     ((B_EN * A_r_val) + (B_IP * B_r_val))
36            0.602615524591539775    ((B_eps_H * B_r_val_1) / (B_r_s_1 * A_cAcB))
37            0.602075050099868836    ((B_IP * B_r_val_1) * (B_r_val_1 * A_cB))
38            0.601878518546554719    ((B_EN + B_IP) * (B_r_val_1 * A_cB))
39            0.601825518602434384    ((B_IP * B_r_val) / sqrt(A_cA))
40            0.601821761937090804    ((B_eps_H * B_r_val_1) / (B_r_s * A_cAcB))
41            0.601344125894804127    ((B_r_val^2) * (B_eps_H * A_cB))
42            0.600970823115835828    ((B_EN + B_IP) * (B_r_val * A_cB))
43            0.600571454496813262    ((B_IP * B_r_val_1) / sqrt(A_cA))
44            0.60011318141145209     ((B_r_val_1^2) * (B_eps_H / A_cAcB))
45            0.599821576463330008    ((B_eps_H / A_cAcB) * (B_r_val_1 * B_r_val))
46            0.599814137722339491    ((B_r_val + A_r_val) * (B_IP / B_r_s))
47            0.598438026132968592    (sqrt(B_IP) * (B_r_val_1 * A_cB))
48            0.598371517938290576    ((B_r_val^2) * (B_eps_H / A_cAcB))
49            0.598064972747625268    ((B_r_val / A_EN) * (B_eps_H + A_eps_H))
#-----------------------------------------------------------------------
50            0.201184375734160259    (|(A_r_val / B_r_val_1) - (B_eps_L / B_eps_H)|)
51            0.200586661035999675    ((A_r_val^3) * (B_EN / B_r_val))
52            0.199613966874044618    ((A_r_val^3) * (B_EN / B_r_val_1))
53            0.198525522293583029    (|(A_r_val / B_r_val) - (B_eps_L / B_eps_H)|)
54            0.197248122005180854    ((B_EN / B_r_val_1) * (|B_r_val_1 - A_r_val|))
55            0.196469665198515886    ((B_EN / B_r_val) * (|B_r_val_1 - A_r_val|))
56            0.196204385752398847    (|(A_EN / B_EN) - (A_r_val / B_r_val_1)|)
57            0.194863312080546469    ((|B_r_val - A_r_val|) * (B_EN / B_r_val_1))
58            0.19440812513410019     ((|B_r_val - A_r_val|) * (B_EN / B_r_val))
59            0.194308573901514875    (|(A_EN / B_EN) - (A_r_val / B_r_val)|)
60            0.19390106819513428     ((|B_r_val_1 - A_r_val|) / (B_r_val_1 * A_IP))
61            0.193809088987817901    ((A_r_val^2) * (B_EN / B_r_val))
62            0.193257448404440979    ((|B_r_val_1 - A_r_val|) / (B_r_val * A_IP))
63            0.193099092889235435    ((A_r_val / B_r_val_1) * (B_EN * A_r_val))
64            0.193055802242054736    ((A_r_val^3) * (B_IP / B_r_val))
65            0.193002616793391646    ((B_IP / B_r_val_1) * (|B_r_val_1 - A_r_val|))
66            0.192707555562955157    ((A_r_val^3) / (B_r_val_1^2))
67            0.192255711030026172    ((A_r_val^3) * (B_IP / B_r_val_1))
68            0.191987841008733146    ((B_IP / B_r_val) * (|B_r_val_1 - A_r_val|))
69            0.191950258111902694    ((|B_r_val - A_r_val|) / (B_r_val_1 * A_IP))
70            0.191912455440904239    ((A_r_s / B_r_val_1) * (|B_r_val_1 - A_r_val|))
71            0.191799057007264684    ((A_r_val^3) / (B_r_val_1 * B_r_val))
72            0.191556813972651202    ((|B_r_val - A_r_val|) / (B_r_val * A_IP))
73            0.191527935437626667    ((|B_r_val - A_r_val|) * (B_IP / B_r_val_1))
74            0.191227797652501946    ((A_r_val / B_r_val) * (B_r_s_1 - A_r_val))
75            0.191151199555967294    ((A_r_s / B_r_val) * (|B_r_val_1 - A_r_val|))
76            0.191080329581551106    ((B_r_val_1 - A_r_val)^2)
77            0.190891572332689219    ((A_r_val^3) / (B_r_val^2))
78            0.190754115664165391    ((|B_r_val - A_r_val|) * (B_IP / B_r_val))
79            0.190629733180367078    ((A_r_val / B_r_val_1) * (|B_r_val_1 - A_r_val|))
80            0.190595703035230574    (|(A_IP / B_EN) - (A_r_val / B_r_val_1)|)
81            0.19001993959003996     ((A_r_val / B_r_val) * (|B_r_val_1 - A_r_val|))
82            0.189670420033550613    ((|B_r_val - A_r_val|) * (A_r_val / B_r_val_1))
83            0.18960201270992072     ((|B_r_val - A_r_val|) * (A_r_s / B_r_val_1))
84            0.189222756246529039    ((|B_r_val - A_r_val|) * (|B_r_val_1 - A_r_val|))
85            0.189214583807410547    ((B_r_val - A_r_val) * (B_r_val_1 - A_r_val))
86            0.189152264768710693    ((|B_r_val - A_r_val|) * (A_r_s / B_r_val))
87            0.189099699665970095    ((|B_r_val - A_r_val|) * (A_r_val / B_r_val))
88            0.18902576753395034     ((A_r_val / B_r_val_1) * (B_r_s_1 - A_r_val))
89            0.188540896218189563    ((A_cB / B_r_val_1) * (|B_r_val_1 - A_r_val|))
90            0.188288587905993898    ((A_r_val^3) / (B_r_s_1 * B_r_val))
91            0.188224013648346761    (|(A_r_s_1 / B_r_s) - (A_r_val / B_r_val_1)|)
92            0.187834311499206486    ((A_cB / B_r_val) * (|B_r_val_1 - A_r_val|))
93            0.187832113706678461    ((|B_r_val_1 - A_r_val|)^3)
94            0.187724765869628191    ((|B_r_val_1 - A_r_val|) / (B_r_val_1 * A_EN))
95            0.187393048854884448    ((A_r_val / B_r_val) * (B_IP + B_eps_L))
96            0.187277756578479643    ((|B_r_val_1 - A_r_val|) / (B_r_val * A_EN))
97            0.187113555247129304    ((|B_r_val - A_r_val|) * (A_cB / B_r_val_1))
98            0.186850975516100803    (|(A_r_val / B_r_val_1) - (B_eps_L / A_eps_H)|)
99            0.186637037621193186    ((|B_r_val_1 - A_r_val|) / (B_r_val_1 * B_r_s_1))
#-----------------------------------------------------------------------
100           0.190368751892649984    ((B_r_val - B_r_s) / (B_EN + B_eps_H))
101           0.188093906420106399    ((B_r_s_1 - B_r_val) / (B_EN + B_eps_H))
102           0.185701151747743681    ((B_r_val_1 - B_r_s) / (B_EN + B_eps_H))
103           0.184623579123209119    ((B_r_val_1 - B_r_s_1) / (B_EN + B_eps_H))
104           0.155587032615970011    ((|B_r_s_1 - B_r_s|) / (B_EN + B_eps_H))
105           0.147848814228768416    ((A_Z / A_cB) / (|B_r_s_1 - B_r_s|))
106           0.146266500960443246    ((A_eps_L * A_Z) / (|B_r_s_1 - B_r_s|))
107           0.145974698093577049    ((B_Z * A_Z) / (B_r_s^3))
108           0.145515679235512979    ((B_Z * A_Z) / (B_r_s^6))
109           0.144798222994012321    ((A_cAcB * A_Z) / (|B_r_s_1 - B_r_s|))
110           0.144506590721450001    ((A_cA * A_Z) / (|B_r_s_1 - B_r_s|))
111           0.143694539207005201    ((B_Z * A_Z) / (B_r_s_1^6))
112           0.143572514879995089    ((|A_r_val_1 - A_r_s_1|) / (|B_r_s_1 - B_r_s|))
113           0.143556719027465057    ((A_Z / B_eps_H) / (|B_r_s_1 - B_r_s|))
114           0.142935886572593623    ((B_Z * A_Z) / (B_r_s_1^3))
115           0.142521730986475137    ((A_Z / B_EN) / (|B_r_s_1 - B_r_s|))
116           0.1424301143446631      ((A_IP * A_Z) / (|B_r_s_1 - B_r_s|))
117           0.141937035754286039    ((A_Z / B_IP) / (|B_r_s_1 - B_r_s|))
118           0.140850036296656683    ((|B_r_val_1 - B_r_s|) / (B_EN + B_eps_H))
119           0.140837743640236795    ((B_r_s_1 * A_Z) / (|B_r_s_1 - B_r_s|))
120           0.140675922232955108    ((B_r_s^6) / (B_EN + B_eps_H))
121           0.140638638743431632    ((A_Z / A_r_s) / (|B_r_s_1 - B_r_s|))
122           0.140499779862578195    ((B_r_s * A_Z) / (|B_r_s_1 - B_r_s|))
123           0.140326793014516865    ((A_EN * A_Z) / (|B_r_s_1 - B_r_s|))
124           0.138620520357043253    (A_Z / (|B_r_s_1 - B_r_s|))
125           0.138591423850191298    ((A_Z * A_r_s_1) / (|B_r_s_1 - B_r_s|))
126           0.13857484649451271     ((A_r_val - A_r_s) / (|B_r_s_1 - B_r_s|))
127           0.138555622818398283    ((B_Z * A_Z) / (B_r_s^2))
128           0.138401584284799484    ((A_Z / A_eps_H) / (|B_r_s_1 - B_r_s|))
129           0.137346719367837405    ((B_r_val_1 * A_Z) / (|B_r_s_1 - B_r_s|))
130           0.137103102485766043    ((A_cA / A_eps_H) / (|B_r_s_1 - B_r_s|))
131           0.136773337229212893    ((B_r_val * A_Z) / (|B_r_s_1 - B_r_s|))
132           0.136666801190018394    ((B_Z * A_Z) / (B_r_s_1 * B_r_s))
133           0.13643942491332739     ((B_Z * A_Z) / (B_EN * B_r_s))
134           0.136356774154203636    ((A_cA^6) / (|B_r_s_1 - B_r_s|))
135           0.136054514904009216    ((A_Z / B_r_s) / (|B_r_s_1 - B_r_s|))
136           0.135682271401153492    ((A_Z / B_r_s_1) / (|B_r_s_1 - B_r_s|))
137           0.13552321950729182     ((B_eps_L * A_Z) / (|B_r_s_1 - B_r_s|))
138           0.135354214697960856    ((B_r_s / B_r_val) / (B_EN + B_eps_H))
139           0.134581374127318854    ((B_Z / A_r_val) / (B_r_s^6))
140           0.134539289769167431    ((B_Z * A_Z) / (B_r_s_1^2))
141           0.134255297890302411    ((B_Z / A_r_val) / (|B_r_s_1 - B_r_s|))
142           0.134157650055273137    ((B_Z * A_Z) / (B_eps_H * B_r_s))
143           0.133906722742593576    ((A_r_s / A_r_val) / (|B_r_s_1 - B_r_s|))
144           0.133901081696753299    ((B_eps_L^3) * (|B_eps_L - B_eps_H|))
145           0.133873854830818761    ((B_r_s^3) / (B_EN + B_eps_H))
146           0.133802677868711611    ((A_cA^3) / (|B_r_s_1 - B_r_s|))
147           0.133624693924397603    ((B_Z * A_Z) / (B_EN * B_r_s_1))
148           0.133544399256785185    ((A_eps_L / A_r_val) / (|B_r_s_1 - B_r_s|))
149           0.133439496708272459    ((B_Z / A_r_val) / (B_r_s^3))
#-----------------------------------------------------------------------
150           0.0994701460697933748   (|(B_EN - B_eps_L) - (B_IP - B_EA)|)
151           0.0811378876650145214   ((B_r_val - A_r_val) / (B_EA * A_r_val))
152           0.0779320472719349233   ((B_r_s - A_r_s_1) * (B_EA / A_r_val_1))
153           0.0776921782978382269   ((B_r_val - A_r_val) / (B_EA * B_Z))
154           0.0776473882948949673   ((B_r_val_1 - A_r_val) / (B_EA * A_r_val))
155           0.0776019499365371151   ((B_EA / A_r_val) / (B_r_s^6))
156           0.0769896251883967209   ((B_EA / A_r_val) / (B_r_s_1^6))
157           0.0764482711049820352   ((B_r_s - A_r_s_1) * (B_EA / A_r_val))
158           0.0760838622687646793   ((B_EA - A_EA) / (B_Z^2))
159           0.0753630233375205127   ((B_EA / A_r_val_1) * (B_r_s_1 - A_r_s_1))
160           0.0752697098089968458   ((|A_r_val_1 - A_r_s_1|) * (B_EA * B_Z))
161           0.0743108686940486401   ((B_EA * B_Z) * (B_Z / A_eps_H))
162           0.0740846594179807166   ((B_r_val_1 - A_r_val) / (B_EA * B_Z))
163           0.0739047340547258347   ((B_EA * A_Z) * (B_Z / B_r_s))
164           0.0735577612092573607   ((B_EN + B_eps_H) / (B_EA * A_r_s_1))
165           0.0727556868325078959   ((B_EA / B_r_s_1) * (B_Z * A_Z))
166           0.0727502512795110551   ((B_r_val - A_r_val_1) / (B_EA * A_r_val))
167           0.0726846495853357422   ((A_cA^2) / (|B_r_s_1 - B_r_s|))
168           0.0722148929923906863   ((A_r_val - A_r_s) * (B_EA * B_Z))
169           0.0720871407394672592   ((A_r_val - A_r_s) * (B_EA * B_r_val))
170           0.0719852645909351274   ((B_r_s - A_r_s) * (B_EA / A_r_val))
171           0.071933841620008751    ((|A_r_val_1 - A_r_s_1|) * (B_EA * B_r_val))
172           0.0719028400993274169   ((A_EN + A_eps_H) * (B_EA * B_r_val))
173           0.07170709396428325     ((B_Z * A_cA) / (|B_r_s_1 - B_r_s|))
174           0.0714396974047405292   ((|A_r_val_1 - A_r_s_1|) * (B_EA / B_r_s))
175           0.0711815482015910883   ((A_r_val - A_r_s) * (B_EA * B_r_val_1))
176           0.071106381964135712    ((|A_r_val_1 - A_r_s_1|) * (B_EA * B_r_val_1))
177           0.0710260133846844777   ((B_EA * B_Z) / (B_r_s * A_r_val))
178           0.0710182457832492925   ((B_r_val - A_r_val) * (B_r_val / B_EA))
179           0.0708348184360934885   ((A_EN + A_eps_H) * (B_EA * B_r_val_1))
180           0.07060561554384992     ((B_EA / A_r_val) * (B_r_s_1 - A_r_s_1))
181           0.0704384503240689935   ((A_r_val - A_r_s) * (B_EA / B_r_s))
182           0.0703279772472434755   ((B_IP * A_Z) / (|B_r_s_1 - B_r_s|))
183           0.0698989495993122129   ((B_EA * B_Z) / (B_r_s_1 * A_r_val))
184           0.0698715373974101162   ((B_EA / A_r_val) / (B_r_s^3))
185           0.0698592395580275721   ((A_r_val_1 - A_r_s_1) * (B_EA * B_r_val))
186           0.0697225562134393545   ((B_r_val_1 - A_r_val_1) / (B_EA * A_r_val))
187           0.0691099030513803864   ((B_EA * A_Z) * (B_Z / A_eps_H))
188           0.0689733431994897367   ((A_cA * A_r_s_1) / (|B_r_s_1 - B_r_s|))
189           0.0689298500723166258   ((B_Z * A_cA) / (B_r_s^6))
190           0.0689081906870284422   ((A_r_val_1 - A_r_s_1) * (B_EA * B_r_val_1))
191           0.0688687241682495316   ((|A_r_val_1 - A_r_s_1|) * (B_EA / B_r_s_1))
192           0.0688553676600044684   ((B_EA / A_r_val_1) * (B_Z * A_Z))
193           0.0687114645055374035   ((A_Z * A_r_s) / (|B_r_s_1 - B_r_s|))
194           0.0686928400894619567   ((B_EA * A_eps_L) * (B_Z * A_Z))
195           0.0685391819841966038   ((A_cA * A_eps_L) / (|B_r_s_1 - B_r_s|))
196           0.068419301337707697    ((B_EA / A_r_val) * (B_Z^2))
197           0.0683017490289361817   ((A_Z / B_r_val_1) / (|B_r_s_1 - B_r_s|))
198           0.0682656209969775135   ((A_cA * A_r_s) / (|B_r_s_1 - B_r_s|))
199           0.0680462799700729676   ((B_EA * A_Z) * (B_Z / B_eps_H))
#-----------------------------------------------------------------------
200           0.0963347541817895642   ((B_eps_H / A_EA) / (B_Z^3))
201           0.0960278596836722231   ((A_eps_L / B_Z) / (B_Z * A_EA))
202           0.0952396526649493674   ((B_eps_L / A_EA) / (B_Z^3))
203           0.0947781553265595461   ((A_cA / A_EA) / (B_Z^2))
204           0.0945568718938789715   ((A_cAcB / B_Z) / (B_Z * A_EA))
205           0.094372020894465225    ((B_r_s / A_EA) / (B_Z^2))
206           0.093079850162094982    ((B_r_s_1 / B_Z) / (B_Z * A_EA))
207           0.0928330647396159031   ((B_IP / B_Z) / (B_Z * A_EA))
208           0.0925864906398579379   ((B_IP / A_EA) / (B_Z^3))
209           0.0921742109281884353   ((B_EN / A_EA) / (B_Z^2))
210           0.0910045539523432617   ((A_IP / B_Z) / (B_Z * A_EA))
211           0.090982570573365687    ((A_r_s / B_Z) / (B_Z * A_EA))
212           0.0908152635069609665   ((B_r_s / A_EA) / (B_Z^3))
213           0.0899422073706724196   ((B_r_s_1 / A_EA) / (B_Z^3))
214           0.0898597341827884893   ((A_EN / A_EA) / (B_Z^2))
215           0.0895044063202926482   ((B_EN / A_EA) / (B_Z^3))
216           0.0893519998143340183   ((B_eps_L / A_EA) / (B_Z^2))
217           0.0890028948680887527   ((A_eps_L / A_EA) / (B_Z^3))
218           0.0881849815382673569   ((B_r_s / B_eps_H) * (|B_r_s_1 - A_r_val|))
219           0.0881231191542315162   ((A_cA / A_EA) / (B_Z^3))
220           0.0878811867780190104   ((B_r_s_1 / B_eps_H) * (|B_r_s_1 - A_r_val|))
221           0.0873396880762134509   ((B_eps_H / A_EA) / (B_Z^2))
222           0.0869265046526776358   ((A_cB / B_Z) / (B_Z * A_EA))
223           0.0865167400824562727   ((A_r_s_1 / B_Z) / (B_Z * A_EA))
224           0.0864894337466663027   ((A_cAcB / A_EA) / (B_Z^3))
225           0.0846113905932579097   ((A_EN / A_EA) / (B_Z^3))
226           0.0843898980770094176   ((B_r_s / B_IP) * (|B_r_s_1 - A_r_val|))
227           0.0836739281062386653   ((|B_r_s_1 - B_r_s|) / (B_Z * A_EA))
228           0.083461113622267824    ((B_r_s_1 / B_IP) * (|B_r_s_1 - A_r_val|))
229           0.0829098824090328951   ((A_cA^6) * (A_r_val / B_Z))
230           0.0823137986881141565   ((|B_r_s_1 - A_r_val|) / (B_IP + A_eps_L))
231           0.0821044787010103416   ((A_r_s / A_EA) / (B_Z^3))
232           0.0819252463440135054   (|(B_r_s * A_r_val) - (B_r_s_1^2)|)
233           0.0818155007401588502   ((A_eps_L / B_eps_H) * (|B_r_s_1 - A_r_val|))
234           0.0817771703215173784   ((A_cA^6) * (A_r_val_1 / B_Z))
235           0.081713204528382391    ((|B_r_s_1 - A_r_val|) / (B_eps_H - A_EA))
236           0.0814875219521945943   ((A_eps_H / B_Z) / (B_Z * A_EA))
237           0.0807191813483243981   ((|B_r_s_1 - A_r_val|) / (B_IP * B_eps_H))
238           0.0806403522263875877   ((A_r_val / B_Z) / (B_Z * A_EA))
239           0.0804933116657846059   (|(A_r_val / B_eps_H) - (B_r_s_1 / B_eps_H)|)
240           0.0803017057265626166   ((|B_r_s_1 - A_r_val|) / (B_IP^2))
241           0.0801296156086347272   ((|B_r_s_1 - A_r_val|) / (B_EN * B_eps_H))
242           0.0799029434044339348   ((|B_r_s_1 - A_r_val|) / (B_IP + A_eps_H))
243           0.0794416186773341526   ((A_r_s / B_eps_H) * (|B_r_s_1 - A_r_val|))
244           0.0793960377067269157   ((|B_r_s_1 - A_r_val|) / (B_IP - B_eps_H))
245           0.0792842796602562516   ((A_cB / A_EA) / (B_Z^3))
246           0.0792569661811841908   ((A_eps_L / B_IP) * (|B_r_s_1 - A_r_val|))
247           0.0792295197473551066   ((|B_r_s_1 - A_r_val|) / (B_EN - B_eps_H))
248           0.0791535072552123631   ((|B_r_s_1 - A_r_val|) / (B_IP + A_EA))
249           0.0790887889944601186   ((B_r_s / B_EN) * (|B_r_s_1 - A_r_val|))
#-----------------------------------------------------------------------
