0             0.756783749144649409    ((B_r_s_1 * lattice_constant_scaled) / sqrt(A_cB))
1             0.752557678368690919    (cbrt(A_cB) * (|B_r_s_1 - lattice_constant|))
2             0.748742982100955468    (cbrt(A_cA) * (B_r_s_1 * lattice_constant_scaled))
3             0.744206762055187543    ((B_r_s * lattice_constant_scaled) / sqrt(A_cB))
4             0.742900475582219277    (sqrt(A_cAcB) * (B_r_s_1 * lattice_constant_scaled))
5             0.742899540974903627    ((|B_r_s - lattice_constant|) * cbrt(A_cB))
6             0.741482877634060844    ((B_r_s * lattice_constant_scaled) * cbrt(A_cA))
7             0.737923170250885385    ((B_r_s_1 * lattice_constant_scaled) / cbrt(A_cB))
8             0.733588065462400851    (cbrt(A_cAcB) * (B_r_s_1 * lattice_constant_scaled))
9             0.73307066350447736     (sqrt(A_cAcB) * (B_r_s * lattice_constant_scaled))
10            0.732423201932134726    (sqrt(A_cB) / (B_r_s_1 * lattice_constant_scaled))
11            0.722466067315054472    ((B_r_s * lattice_constant_scaled) / cbrt(A_cB))
12            0.721309528965097257    ((A_r_s * lattice_constant_scaled) * (B_r_s_1 * A_IP))
13            0.719964037979175187    (cbrt(A_cAcB) / (|B_r_s - lattice_constant|))
14            0.719856934242643631    (cbrt(A_cAcB) * (B_r_s * lattice_constant_scaled))
15            0.71898546259826901     ((B_r_s_1 * lattice_constant_scaled) / (A_cB * lattice_constant))
16            0.71874520539701281     ((|B_r_s - lattice_constant|) / cbrt(A_cAcB))
17            0.71852520966873723     ((|B_r_s_1 - lattice_constant|) / cbrt(A_cAcB))
18            0.71825398905678195     (cbrt(A_cAcB) / (|B_r_s_1 - lattice_constant|))
19            0.717877124847270731    ((A_cB * lattice_constant) / (B_r_s_1 * lattice_constant_scaled))
20            0.716611486160990352    ((B_r_s * lattice_constant_scaled) / (A_cB * lattice_constant))
21            0.709303762877211308    ((A_cB / B_r_s_1) * (lattice_constant^6))
22            0.708780564248566058    ((A_IP * A_r_s) * (B_r_s * lattice_constant_scaled))
23            0.706823997694565831    (sqrt(A_cB) / (B_r_s * lattice_constant_scaled))
24            0.705562257963605499    ((A_cAcB / lattice_constant_scaled) / (B_r_s_1 * A_cA))
25            0.705329790106974719    ((A_cB * lattice_constant) / (B_r_s * lattice_constant_scaled))
26            0.70476853418106522     ((B_r_s_1 * lattice_constant_scaled) / (A_r_val + lattice_constant))
27            0.704458318280000384    (cbrt(A_cB) / (B_r_s_1 * lattice_constant_scaled))
28            0.701002777562507728    ((A_cB / lattice_constant_scaled) / (B_r_s_1 + B_r_s))
29            0.700996140142427504    ((A_EN * lattice_constant_scaled) * (B_r_s_1 / A_IP))
30            0.700535605903620517    ((|B_r_s - lattice_constant|) * sqrt(A_cB))
31            0.699801131199065618    ((B_r_s * lattice_constant_scaled) * sqrt(A_cA))
32            0.699087361300015142    ((B_r_s * lattice_constant_scaled) / (A_r_val + lattice_constant))
33            0.698876469123701716    (sqrt(A_cA) * (B_r_s_1 * lattice_constant_scaled))
34            0.697290467435169536    ((A_IP / B_r_s_1) / (A_EN * lattice_constant_scaled))
35            0.697000755733165223    ((B_r_s_1 * A_cAcB) / (lattice_constant^6))
36            0.696692204942715221    ((lattice_constant_scaled / A_cB) * (B_r_s_1 + B_r_s))
37            0.696671434083063978    ((A_cAcB / lattice_constant) * (B_r_s_1 * lattice_constant_scaled))
38            0.696345498631064874    ((B_r_s / A_cB) * lattice_constant_scaled)
39            0.696241786768651227    ((A_cAcB / lattice_constant) * (B_r_s * lattice_constant_scaled))
40            0.695857562059635115    ((A_cB / B_r_s) * (lattice_constant^6))
41            0.695772007370923373    ((A_cA * lattice_constant_scaled) * (B_r_s_1 / A_cAcB))
42            0.695680609379155213    ((A_cAcB / B_r_s) / (A_cA * lattice_constant_scaled))
43            0.694559980135347144    ((B_r_s * A_cAcB) / (lattice_constant^6))
44            0.693355878057947739    ((B_IP * A_cB) * (B_r_val / lattice_constant_scaled))
45            0.692907544505009021    ((A_EN * lattice_constant_scaled) * (B_r_s / A_IP))
46            0.692697455755683977    (sqrt(A_cB) * (|B_r_s_1 - lattice_constant|))
47            0.690933410155056937    ((A_cB / B_r_s_1) * (lattice_constant^3))
48            0.690080518559558764    ((A_r_val + lattice_constant) / (B_r_s_1 * lattice_constant_scaled))
49            0.686994104606180711    ((A_cB / lattice_constant_scaled) * (B_IP * B_r_val_1))
#-----------------------------------------------------------------------
50            0.302853217779101291    ((B_IP / lattice_constant_scaled) / sqrt(B_Z))
51            0.299324917738344576    ((lattice_constant_scaled / B_IP) * sqrt(B_Z))
52            0.289139706203800695    ((B_IP / lattice_constant_scaled) / cbrt(B_Z))
53            0.278712934834582815    ((lattice_constant_scaled / B_IP) * cbrt(B_Z))
54            0.275697158465295911    ((lattice_constant_scaled^2) * (B_Z / B_eps_H))
55            0.274562802672963902    ((lattice_constant_scaled / A_r_s) * (B_Z + A_Z))
56            0.274476283632440698    ((B_Z / B_IP) / (lattice_constant^6))
57            0.273145142893382376    ((B_eps_H * A_cB) / (B_Z * lattice_constant_scaled))
58            0.269806064024275316    ((lattice_constant_scaled / B_EN) * cbrt(B_Z))
59            0.269354266279125842    ((B_EN / lattice_constant_scaled) / (B_Z + A_Z))
60            0.268573771415514639    ((B_eps_H / B_Z) / (A_cAcB * lattice_constant_scaled))
61            0.26658870001567142     ((B_EN / lattice_constant_scaled) / cbrt(B_Z))
62            0.263078551617775824    ((lattice_constant_scaled / B_EN) * (B_Z * lattice_constant_scaled))
63            0.26054174858526824     ((B_EN / B_Z) / (A_cAcB * lattice_constant_scaled))
64            0.260344125976827878    ((B_IP * A_cB) / (B_Z * lattice_constant_scaled))
65            0.260285693105893501    ((B_EN * A_cB) / (B_Z * lattice_constant_scaled))
66            0.258424189798765191    ((lattice_constant_scaled / B_EN) * sqrt(B_Z))
67            0.258362586481846901    ((B_Z + A_Z) / (lattice_constant^6))
68            0.257923628263862947    ((B_Z / B_eps_H) / (lattice_constant^6))
69            0.257324193654420719    ((lattice_constant_scaled / B_eps_H) * (B_Z + A_Z))
70            0.257284553663813365    ((B_IP / A_cAcB) / (B_Z * lattice_constant_scaled))
71            0.256478259590435631    ((B_EN / lattice_constant_scaled) / sqrt(B_Z))
72            0.255928394501278522    ((B_eps_H / lattice_constant_scaled) / (B_Z + A_Z))
73            0.254874599998405837    ((lattice_constant_scaled / B_IP) * (B_Z + A_Z))
74            0.253929626963309674    ((A_cAcB / B_eps_H) * (B_Z * lattice_constant_scaled))
75            0.253053349160521024    ((lattice_constant_scaled / B_EN) * (B_Z + A_Z))
76            0.253005324266218301    ((B_eps_H + A_EA) / (B_Z * lattice_constant_scaled))
77            0.252676399857921063    ((A_cAcB * lattice_constant_scaled) * (B_Z * lattice_constant_scaled))
78            0.252237344188935786    ((A_IP * lattice_constant_scaled) * (B_Z + A_Z))
79            0.252141394119212714    ((lattice_constant_scaled^2) * (B_Z + A_Z))
80            0.250829835063398288    ((B_eps_H / B_Z) / (lattice_constant_scaled^2))
81            0.250778380061010331    ((B_IP / lattice_constant_scaled) / (B_Z + A_Z))
82            0.250245974135496374    ((B_EN * B_IP) / (B_Z * A_cA))
83            0.250153817043205606    ((B_EN * B_eps_H) / (B_Z * A_cA))
84            0.249954343386941946    ((B_EN^2) / (B_Z * A_cA))
85            0.249863554267084398    ((lattice_constant_scaled^2) * (B_Z / A_cB))
86            0.249685154808060916    ((B_EN / lattice_constant_scaled) / (B_Z * A_cA))
87            0.248286549546903379    ((lattice_constant_scaled / lattice_constant) * (B_Z + A_Z))
88            0.247785414263128612    ((B_Z * A_cAcB) / (lattice_constant^6))
89            0.247607427559790705    ((A_r_s * lattice_constant_scaled) * (B_Z * lattice_constant_scaled))
90            0.24735166978144596     ((B_IP / B_Z) / (lattice_constant_scaled^2))
91            0.247021011805755336    ((B_EN * A_cB) / sqrt(B_Z))
92            0.245285895551732663    ((A_r_s / lattice_constant_scaled) / (B_Z + A_Z))
93            0.243171021669652099    ((B_r_s^6) / (|B_r_val_1 - B_r_s|))
94            0.242799175978156867    ((B_Z / A_cA) / (B_r_s_1 - B_r_val))
95            0.242767778941626128    ((B_Z * A_cB) / (B_r_s_1 - B_r_val))
96            0.242667836144399657    ((B_eps_H / B_Z) / (A_cA * lattice_constant_scaled))
97            0.242584474097895852    ((B_Z / A_cB) / (lattice_constant^6))
98            0.242541229631997868    ((A_cAcB / B_IP) * (B_Z * lattice_constant_scaled))
99            0.242479776112468015    ((B_IP^3) / (B_Z * A_cA))
#-----------------------------------------------------------------------
100           0.201215166785407351    ((A_r_val - lattice_constant) * (B_r_val_1 * B_r_s))
101           0.1975433342778595      ((A_r_val - lattice_constant) * (B_r_val * B_r_s))
102           0.196861595008695239    ((A_r_val - lattice_constant) * (B_r_val_1 * B_r_s_1))
103           0.193102981346819774    ((A_r_val - lattice_constant) * (B_r_s_1 * B_r_val))
104           0.188969169335113846    ((B_r_val_1 * B_r_s) / (A_r_val + A_r_s))
105           0.185089037747223822    ((B_r_val_1 * B_r_s_1) / (A_r_val + A_r_s))
106           0.184538588151583638    (cbrt(B_Z) * lattice_constant_scaled)
107           0.18400765696644969     ((B_r_val * B_r_s) / (A_r_val + A_r_s))
108           0.181395465214532814    ((A_cAcB * lattice_constant_scaled) * (|B_r_s - A_r_s_1|))
109           0.180715360049103935    (|(|B_r_s - A_r_s|) - (B_r_val_1 + lattice_constant)|)
110           0.180626748321786879    ((B_r_val_1 * B_r_s_1)^6)
111           0.180563448553858641    ((A_cB * lattice_constant_scaled) * (B_Z + A_Z))
112           0.180258290714249064    ((A_IP + A_eps_H) * (B_r_val_1 * B_r_s))
113           0.180217637578741402    (sqrt(B_Z) / (lattice_constant^6))
114           0.180048354610125166    ((B_r_s_1 * B_r_val) / (A_r_val + A_r_s))
115           0.179117290291070125    ((A_r_s_1 / B_Z) / (B_r_s^6))
116           0.178973308850009921    ((B_r_val_1 * B_r_s)^6)
117           0.178876421716695294    ((|B_r_s - A_r_s_1|) * (A_r_s_1 / B_Z))
118           0.178750379293520639    ((A_r_val - lattice_constant) / (B_IP * lattice_constant_scaled))
119           0.17858745687896338     ((|A_r_s - lattice_constant|) / (B_IP - A_eps_H))
120           0.17765345024112858     ((|B_r_s - A_r_s|) - (B_r_val + lattice_constant))
121           0.177533822664709434    ((|B_r_s - A_r_s_1|) * (A_Z / B_Z))
122           0.177126672147415565    ((lattice_constant^6) / sqrt(B_Z))
123           0.177109389989714233    ((lattice_constant_scaled / lattice_constant) * sqrt(B_Z))
124           0.17691890298023874     ((lattice_constant^6) / (A_r_val + A_r_s))
125           0.176547122897624281    ((|B_r_s - A_r_s_1|) * (A_r_s_1 * lattice_constant_scaled))
126           0.176495836498547137    ((A_IP + A_eps_H) * (B_r_val_1 * B_r_s_1))
127           0.17616454840363685     (cbrt(B_Z) / (lattice_constant^3))
128           0.174873875191977918    ((lattice_constant / lattice_constant_scaled) / (A_r_val + A_r_s))
129           0.174749040338230605    ((A_IP + A_eps_H) * (B_r_val * B_r_s))
130           0.174557326518066019    ((A_r_s_1 / B_Z) / (B_r_s_1^6))
131           0.173281229092261319    ((A_r_val - lattice_constant) / (B_eps_H * lattice_constant_scaled))
132           0.172519067399105247    ((|B_r_s - A_r_s_1|) * (A_cAcB / B_Z))
133           0.172450970871349002    ((lattice_constant / lattice_constant_scaled) / cbrt(B_Z))
134           0.172339112464802852    ((A_eps_H / A_cA) / (B_r_val_1 - B_r_s))
135           0.172041120368602751    ((B_r_val_1 * B_r_s) / (A_cB * lattice_constant_scaled))
136           0.171411001979964506    ((A_r_val - lattice_constant) * (B_r_val + B_r_s))
137           0.171270057660469377    ((|B_r_s - A_r_s_1|) * (A_cA / B_Z))
138           0.171130265368411832    ((A_r_val - lattice_constant) * (B_r_s_1 + B_r_val))
139           0.171125343272667735    ((lattice_constant_scaled / lattice_constant) * cbrt(B_Z))
140           0.170944971305818899    ((A_IP + A_eps_H) * (B_r_s_1 * B_r_val))
141           0.17052283803855206     ((A_cA / B_Z) / (B_r_s^6))
142           0.170370845661317633    ((B_EN - B_eps_L) / (|A_r_s - lattice_constant|))
143           0.170334111972326369    ((B_r_val * B_r_s) / (A_cB * lattice_constant_scaled))
144           0.169914596037338439    ((A_cA / B_Z) / (B_r_s_1^6))
145           0.169848716784581716    ((A_cB * A_eps_H) / (B_r_val_1 - B_r_s))
146           0.169789813352373559    ((|A_r_s - lattice_constant|) * sqrt(A_r_s))
147           0.169335075589162276    ((A_r_val / A_r_s) / (B_r_val_1 - B_r_s))
148           0.169144174728752372    ((B_r_s * A_EN) * (B_r_val_1 / lattice_constant_scaled))
149           0.169131293095022589    ((A_r_val - lattice_constant) * (B_r_val_1 + B_r_s_1))
#-----------------------------------------------------------------------
150           0.16112925718617177     ((A_Z / B_Z) - (B_r_val_1 / A_r_val))
151           0.160939968033144615    ((B_r_s_1 * A_r_val) * (B_Z + A_Z))
152           0.156453773549837749    ((B_r_s * A_r_val) * (B_Z + A_Z))
153           0.155739312722821904    ((A_Z / B_Z) - (B_r_val / A_r_val))
154           0.155516145612192874    ((A_r_val - lattice_constant) * (|A_r_s - lattice_constant|))
155           0.154919198403167924    ((|B_r_s - A_r_s|) - (|B_r_s_1 - lattice_constant|))
156           0.154481993555340091    ((B_IP / lattice_constant_scaled) / (A_r_val + A_r_s))
157           0.152281616550183629    ((|A_r_s - lattice_constant|) / (A_eps_H * A_r_s))
158           0.149257935537848968    ((lattice_constant^3) / (A_r_val + A_r_s))
159           0.148469988905056527    ((A_r_val - lattice_constant) / (A_r_s * lattice_constant_scaled))
160           0.148029779667780415    ((A_IP / A_r_s) / (A_eps_H * lattice_constant_scaled))
161           0.147323926033022967    ((A_r_val / B_r_s) + (A_Z / B_Z))
162           0.146482543848360791    ((B_r_s_1^3) * (B_Z * A_r_val))
163           0.146283781860647277    ((B_eps_L * B_r_val_1) * (B_r_val_1 - A_r_val))
164           0.14600758862493235     ((A_Z * lattice_constant_scaled) * (B_r_s_1 * A_r_val))
165           0.145832831380516531    ((A_r_val + A_r_s) / (B_IP + B_eps_H))
166           0.145575987200923629    ((B_r_val - A_r_val) * (B_Z * B_r_val_1))
167           0.14525510630449287     ((B_eps_L * B_r_val_1) * (B_r_val - A_r_val))
168           0.144683536625030307    ((A_r_val / B_r_s_1) + (A_Z / B_Z))
169           0.144599674929423272    ((A_Z * A_r_val) * (B_EA - B_eps_L))
170           0.14447060103791512     ((A_IP / A_eps_H) + (A_Z / B_Z))
171           0.144016800585434035    ((B_r_val - A_r_val) * (B_Z * B_r_val))
172           0.143965927323872672    ((B_eps_L * B_r_val) * (B_r_val_1 - A_r_val))
173           0.143651061841036598    ((A_Z * A_r_val) * (|B_r_s_1 - A_r_s_1|))
174           0.143012884901209253    ((B_eps_L * B_r_val) * (B_r_val - A_r_val))
175           0.142889761385689817    ((|B_r_s - A_r_s_1|) * (A_Z * A_r_val))
176           0.142743180688589599    ((A_r_val * lattice_constant_scaled) * (B_Z + A_Z))
177           0.142404572592479517    ((A_r_s^3) * (A_eps_H * lattice_constant_scaled))
178           0.142290185795040575    ((B_Z * B_r_val_1) * (B_r_val_1 - A_r_val))
179           0.141984915515144167    ((|A_r_s - lattice_constant|) / cbrt(A_r_val))
180           0.141866398635942109    ((A_Z * A_r_val) / (B_IP + B_eps_H))
181           0.141808161072624256    ((B_Z * A_Z) * (B_r_val_1 - A_r_val))
182           0.141510679673696321    (sqrt(A_Z) * (B_r_s_1 * A_r_val))
183           0.141164170342758688    ((A_IP + A_eps_H) * (B_IP / lattice_constant_scaled))
184           0.141136181760023321    ((A_cAcB * A_cB) / (|A_r_s - lattice_constant|))
185           0.141009779884055469    ((A_r_val / lattice_constant) * (B_r_s_1 * A_Z))
186           0.14100541543636988     ((B_r_s_1 * A_Z) * (B_eps_H * A_r_val))
187           0.140779801730365695    ((B_r_s * A_r_val) * (A_Z * lattice_constant_scaled))
188           0.140693110189212711    ((B_r_s_1 + A_r_val) * (B_Z + A_Z))
189           0.140624242392328291    ((B_Z * B_r_val) * (B_r_val_1 - A_r_val))
190           0.140465083573443617    ((B_r_s_1^6) * (B_Z * A_r_val))
191           0.14045411395597715     ((B_r_val - A_r_val) * (B_Z * A_Z))
192           0.140049194742548372    ((A_IP / A_eps_H) * (|A_r_s - lattice_constant|))
193           0.140046217998155448    ((A_Z * A_r_val) - (B_Z * A_r_s_1))
194           0.1399795598128174      ((A_Z * A_r_val) * (B_r_s_1^2))
195           0.139829290111244675    ((B_r_s + A_r_val) * (B_Z + A_Z))
196           0.139450904538532011    ((A_Z / B_r_s_1) - (B_Z / A_r_val))
197           0.13923513735408774     ((A_r_val / B_IP) * (A_Z * lattice_constant_scaled))
198           0.139097933236665749    ((A_r_val / B_IP) * (B_eps_H * A_Z))
199           0.139001386503485425    ((B_r_s * A_Z) + (B_Z * A_r_val))
#-----------------------------------------------------------------------
200           0.131166162830038568    ((B_r_s_1^6) * (B_Z / A_cA))
201           0.127186819372572091    ((B_r_s_1^6) * (B_r_val_1 * A_r_val))
202           0.125721178795489719    ((B_r_s_1^6) * (B_r_val * A_r_val))
203           0.125596609236841406    ((B_r_s_1 * A_cB) / (B_IP + B_eps_H))
204           0.119137129103697337    ((A_r_val / B_EN) * (B_r_s_1^6))
205           0.11853344752971913     ((B_eps_L * A_r_val) * (B_r_s_1^6))
206           0.118290580910324086    ((B_r_s_1^3) * (B_r_val_1 * A_cB))
207           0.118158267565130884    ((B_r_s_1^6) * (B_eps_H * A_r_val))
208           0.11800264333296398     ((A_eps_H / A_cA) * (B_r_s_1^6))
209           0.117710061743280844    ((B_r_s_1^3) * (B_r_val_1 * A_r_val))
210           0.117513019708630012    ((B_r_s_1^6) * (B_r_val_1 / A_cA))
211           0.116526841243667703    ((|B_r_s_1 - B_r_s|) / (B_r_s_1^6))
212           0.115967443422269889    ((B_r_s_1 / A_cAcB) / (B_IP + B_eps_H))
213           0.115938554691901397    ((B_r_s_1^6) * (B_r_val / A_cA))
214           0.115752779092004848    ((B_EA / B_r_val_1)^6)
215           0.115357960417085639    ((B_r_s * A_cB) / (B_IP + B_eps_H))
216           0.115181168911733606    ((|B_r_s_1 - B_r_s|) / (B_r_s_1^3))
217           0.114975061873525936    (|(A_EA^6) - (B_EA^6)|)
218           0.114847871952851205    ((B_EA / B_r_val)^6)
219           0.114152376665861324    ((A_EA^6) + (B_EA^6))
220           0.113842841781317899    ((A_r_val_1 / A_r_s_1) * (B_r_s_1^6))
221           0.113320589163236257    ((B_r_s^6) * (B_Z * A_r_val))
222           0.112852488008776106    ((B_r_s_1^6) * (B_Z * A_cB))
223           0.112561652837078724    ((A_r_val / A_r_s) * (B_r_s_1^6))
224           0.112303424046531569    ((B_eps_L / A_cA) * (B_r_s_1^6))
225           0.112182540336508069    ((A_EA + A_eps_H) * (B_r_s_1^6))
226           0.112146644872883047    ((B_eps_H / A_cA) * (B_r_s_1^6))
227           0.112123537424973496    ((A_cB * A_r_val) * (B_r_s_1^6))
228           0.112123165847195935    ((B_r_s_1^6) / (B_EN * A_cA))
229           0.112117575040672809    ((B_r_s_1^3) * (B_r_val * A_r_val))
230           0.112062091770793029    ((A_cB^3) * (B_r_s_1^6))
231           0.111970367777707358    ((B_r_s_1^3) * (B_r_val * A_cB))
232           0.111793711183273525    ((|B_r_s_1 - B_r_s|) / (B_r_s^3))
233           0.111671101702639622    ((A_r_val / B_IP) * (B_r_s_1^6))
234           0.111077995162585028    ((|B_IP - A_IP|) / (B_EN + B_eps_H))
235           0.109945892993829689    ((B_r_s_1^6) * (B_Z / A_cAcB))
236           0.109648313432190628    (sqrt(A_cB) / (B_IP + B_eps_H))
237           0.109399565679892133    ((B_r_val_1 + B_r_s_1) / (B_r_val + B_r_s))
238           0.109223148828360248    ((B_r_val + B_r_s) / (B_r_val_1 + B_r_s_1))
239           0.10919816996492672     ((B_r_s_1^6) * (B_r_s_1 * A_r_val))
240           0.109145769457446515    ((A_r_val / B_r_val_1) * (|B_r_s_1 - B_r_s|))
241           0.108558051638248595    ((B_IP + B_eps_H) / (B_r_val_1 + B_r_s_1))
242           0.108446733894008732    ((B_r_s_1^3) * (B_r_val_1 / A_EN))
243           0.108406163190183077    ((A_r_val / A_cAcB) * (B_r_s_1^6))
244           0.10834928370533177     ((A_r_val / lattice_constant) * (B_r_s_1^6))
245           0.108343751097261479    ((A_r_val / B_r_val) * (|B_r_s_1 - B_r_s|))
246           0.108066625825810031    ((A_r_val * lattice_constant_scaled) * (B_r_s_1^6))
247           0.107945773676559248    ((B_IP + B_eps_H) / (B_r_val_1 + B_r_s))
248           0.107817446583365686    ((B_IP * A_r_val) / (B_EN + B_eps_H))
249           0.10771550650463052     ((B_IP / B_eps_H) / (B_r_val_1 + B_r_s_1))
#-----------------------------------------------------------------------
