0             0.649996153352530914    (B_Z * B_r_val_1)
1             0.639430427872778706    (B_Z * B_r_val)
2             0.592531910623041669    (B_Z * A_r_s)
3             0.549044733527148443    (B_Z / B_r_s)
4             0.540927116376337613    (B_Z / B_r_s_1)
5             0.534314761555839568    (B_Z / B_IP)
6             0.527412790944937759    (B_Z / A_IP)
7             0.525128025266032528    sqrt(B_Z)
8             0.523827654040644575    cbrt(B_Z)
9             0.514938130727303101    B_Z
10            0.494085335418328886    (B_Z / B_EN)
11            0.471778286066321606    (B_IP / B_Z)
12            0.47120071185132173     (A_IP / B_Z)
13            0.461581031072145154    (B_Z^2)
14            0.442482692708747849    (B_Z / A_eps_H)
15            0.439875623376592639    (B_Z + A_Z)
16            0.439717842152182126    (B_Z / B_eps_H)
17            0.438211165788527868    (A_EN / B_Z)
18            0.422326104758966481    (B_eps_H / B_Z)
19            0.422242977990798851    (B_eps_L / B_Z)
20            0.420994857838735803    (B_Z / A_EN)
21            0.420867481665478493    (B_eps_H * B_Z)
22            0.419872432233291182    (B_EN * B_Z)
23            0.417324963977374286    (B_IP * B_Z)
24            0.411490049539381508    (B_Z * A_cB)
25            0.406852825055023348    (B_r_s / B_Z)
26            0.403442570319732574    (1.0 / B_Z)
27            0.396642124587660405    (B_Z / A_cB)
28            0.393516357633742486    (B_r_s_1 / B_Z)
29            0.383079585938583989    (B_Z * B_r_s_1)
30            0.379494269811757712    (B_Z * A_cAcB)
31            0.372372674440231644    (A_IP / B_r_val_1)
32            0.37176485343722393     (B_Z / A_cAcB)
33            0.366908680526157549    (A_cAcB / B_Z)
34            0.36605869043751621     (B_r_val_1 * A_r_s)
35            0.366058652360053416    (A_eps_H / B_Z)
36            0.363159735209772683    (B_r_val_1 + A_r_s)
37            0.360965984715275856    (A_IP / B_r_val)
38            0.360302550940576338    (B_Z * A_eps_L)
39            0.360164472317647866    (B_r_val_1 + B_r_s_1)
40            0.354545187832828512    (B_r_val_1 * B_r_s_1)
41            0.353699449267448396    (B_EN / B_Z)
42            0.348849608339108719    (B_Z * B_r_s)
43            0.346868713814870111    (B_Z * A_Z)
44            0.346820165936948754    (A_eps_L / B_Z)
45            0.34062217814834217     (B_r_val + A_r_s)
46            0.336727836329130781    (A_EN / B_r_val_1)
47            0.335028934564126968    (B_r_val_1 + B_r_s)
48            0.333189171599851175    (B_r_val_1 * B_r_s)
49            0.330941401895708365    (B_r_val_1 / A_IP)
#-----------------------------------------------------------------------
50            0.450890173946221384    (B_r_s_1 + A_r_s)
51            0.440752980614162293    (B_r_s + A_r_s)
52            0.433564945798051049    (B_r_s_1 * A_r_s)
53            0.411285955592907426    (B_r_s * A_r_s)
54            0.364959398305598481    (|B_r_s_1 - A_r_val_1|)
55            0.348418834481161288    (B_r_val * A_r_s)
56            0.33359099225777511     (A_Z * A_r_val)
57            0.325455514317163241    (A_cB * A_Z)
58            0.323864390471707897    (B_r_val_1 + B_r_s_1)
59            0.321557893694185359    (A_Z / A_cA)
60            0.317971160386079421    (A_r_s^3)
61            0.316806479412698516    (A_r_s^2)
62            0.310530833970565912    (B_r_s_1 + B_r_val)
63            0.309696982637916118    (B_r_val / A_IP)
64            0.30702498170511211     (A_IP / B_r_s_1)
65            0.305388304101746166    (A_Z / A_cAcB)
66            0.30537980750533994     (A_Z / A_EN)
67            0.303970231257260359    A_r_s
68            0.302416723215454875    (B_r_s_1 / A_IP)
69            0.29903031015114101     (B_r_val + B_r_s)
70            0.29740103342482116     (B_EN * A_r_s)
71            0.293067849508323264    sqrt(A_r_s)
72            0.291762619344454244    (B_r_s / A_IP)
73            0.291235784302436629    (A_IP / B_r_s)
74            0.288837368884997947    cbrt(A_r_s)
75            0.280071309681666658    (A_r_s^6)
76            0.276243529806029564    (B_r_val_1 * A_Z)
77            0.273419672599547858    (B_r_s_1 * B_r_val)
78            0.273313185909619616    (B_r_val * A_Z)
79            0.270252809805498595    (A_Z / A_IP)
80            0.268503631696280554    (B_r_val_1 * A_cAcB)
81            0.263760142905608508    (A_r_s / B_IP)
82            0.262918698503697978    (B_r_val_1 / A_EN)
83            0.260771317257618152    (B_r_val * B_r_s)
84            0.259364258906691958    (B_r_val / A_EN)
85            0.254507633989524862    (B_r_val_1 * A_r_s_1)
86            0.252937497047155624    (|B_r_s - A_r_s|)
87            0.251025613069268738    (B_r_s_1 * A_Z)
88            0.249648443459652314    (B_r_val * A_cAcB)
89            0.247675236329461007    (B_r_s * A_Z)
90            0.247516248771036618    (1.0 / A_r_s)
91            0.246477808413741728    (B_IP * A_r_s)
92            0.244245247502624302    (A_EN / B_r_val)
93            0.244075456822455378    (A_Z * A_r_s)
94            0.242476254401247399    (B_r_val * A_r_s_1)
95            0.241485943611631726    (B_r_val_1 / A_eps_H)
96            0.240779725075858264    (A_cAcB / A_EN)
97            0.240528788493451801    (|B_r_s_1 - A_r_s|)
98            0.239928105967089966    (B_r_s - A_r_s)
99            0.239866538754288594    (B_eps_H * A_r_s)
#-----------------------------------------------------------------------
100           0.28997010739966661     (B_r_s_1^6)
101           0.286877357530090915    (B_r_s^6)
102           0.233681884962613079    (B_r_s_1^3)
103           0.218564845079731868    (B_r_s^3)
104           0.21359010042018936     (B_r_s_1^2)
105           0.204685378752811481    (B_r_s_1 * B_r_s)
106           0.195270622625534407    (B_r_s^2)
107           0.193850539847590819    B_r_s_1
108           0.184298286642165993    sqrt(B_r_s_1)
109           0.183939096970859806    (B_r_s_1 * A_cB)
110           0.183311092263597986    (B_r_s_1 + B_r_s)
111           0.181177662755405755    cbrt(B_r_s_1)
112           0.179504720767388937    (B_r_s * A_cB)
113           0.173002672323114587    B_r_s
114           0.16310163697181998     (B_r_s + A_r_val)
115           0.162461852384329947    sqrt(B_r_s)
116           0.161777819685629765    (B_EN * B_r_s_1)
117           0.159597675915208598    (B_r_s * A_r_val)
118           0.159499467081303359    (B_r_s_1 + A_r_val)
119           0.159053298534096305    cbrt(B_r_s)
120           0.158786224510154061    (B_r_s_1 * A_r_val)
121           0.158611555433905704    (B_r_s_1 / A_cB)
122           0.157642083787398218    (1.0 / B_r_s_1)
123           0.152320552954760796    (B_EN * B_r_s)
124           0.151684926699175771    (B_r_s / A_cB)
125           0.146413331071268193    (B_r_s_1 / A_eps_H)
126           0.145372164825400857    (A_cAcB / B_r_s_1)
127           0.142956001580798991    (B_r_s / A_eps_H)
128           0.137641671958918937    (A_cAcB / B_r_s)
129           0.136638098780552553    (B_IP * B_r_s_1)
130           0.13494180411699358     (A_eps_H / B_r_s_1)
131           0.133945399461504661    (1.0 / B_r_s)
132           0.133107776117147081    (A_eps_H / B_r_s)
133           0.126677356251637174    (A_cB / B_r_s_1)
134           0.126639475483505404    (B_r_s + A_r_val_1)
135           0.124516972300396905    (B_r_s_1 * A_cAcB)
136           0.123515944452240525    (B_r_s_1 + A_r_val_1)
137           0.123104407952397646    (B_IP * B_r_s)
138           0.121937237268522111    (A_cA / B_r_s_1)
139           0.121582233629679431    (B_r_s_1 / A_cAcB)
140           0.121430466056968125    (A_cB / B_r_s)
141           0.121025020927098867    (A_EN / B_r_s_1)
142           0.120356191052900074    (B_r_s / A_cAcB)
143           0.119694966769780989    (B_r_s * A_cAcB)
144           0.118647509196306553    (A_cA / B_r_s)
145           0.117687939173745926    (B_EN * A_r_val)
146           0.117383617446748675    (A_EN / B_r_s)
147           0.116392805288367568    (B_eps_L * A_eps_L)
148           0.108912816865399442    (A_cAcB * A_cB)
149           0.108177370097744294    (B_r_s * A_r_val_1)
#-----------------------------------------------------------------------
150           0.15463237354132639     (B_EN + A_eps_L)
151           0.13826096044776065     (A_r_val_1 / B_eps_L)
152           0.133777257104763764    (A_eps_L / B_EN)
153           0.132458399299487772    (B_IP + A_eps_L)
154           0.131610724105166638    (B_eps_L / B_eps_H)
155           0.127956686860138474    (B_eps_L / A_cAcB)
156           0.127129757369526808    (B_IP + B_eps_L)
157           0.124268918272077658    (B_eps_H - A_eps_L)
158           0.122424042111901388    (|B_eps_H - A_eps_L|)
159           0.118695861765199959    (|B_eps_L - B_eps_H|)
160           0.116244313099804555    (B_IP - A_eps_H)
161           0.115391172393502037    (A_eps_L / B_IP)
162           0.114656728895945997    (A_Z * A_r_val_1)
163           0.113895805548966042    (B_eps_L / A_cA)
164           0.112559942353403858    (B_EN - A_eps_H)
165           0.112549082469612621    (A_r_val / B_eps_L)
166           0.112492690298223494    (B_EN / A_eps_L)
167           0.112370508705605318    (B_eps_L + A_eps_L)
168           0.110593189514794327    (B_EA / A_EA)
169           0.109463683893995328    (B_EN * A_r_val_1)
170           0.106061181764939838    (B_eps_H + A_eps_H)
171           0.105499337761874881    (B_EN * B_IP)
172           0.105244497501937359    (B_EN + B_IP)
173           0.104891082572919334    (A_cA * A_r_val_1)
174           0.104400113173190923    (A_eps_L / B_eps_H)
175           0.104006903512253654    (B_EN * B_eps_H)
176           0.10371647191111695     (B_eps_L - A_eps_H)
177           0.103362681380311378    (B_eps_L / B_IP)
178           0.102359588815609026    (B_Z / A_EA)
179           0.102149423745282839    (B_eps_H / A_eps_L)
180           0.102124253409646315    (B_EN - B_eps_H)
181           0.101995352722321478    (A_EA * A_r_val_1)
182           0.101591470111666374    (B_eps_L / A_EN)
183           0.101405641624464957    (A_eps_H / B_eps_L)
184           0.10060840074648246     (B_eps_L / A_EA)
185           0.100504799693239819    (A_r_val_1 / A_EA)
186           0.0999613853882110881   (B_IP + A_EA)
187           0.0992324162740210181   (B_EN * A_eps_H)
188           0.0991607214379432306   (B_r_s / B_eps_L)
189           0.098666764268640017    (B_eps_H * A_r_val_1)
190           0.0983028946224382      (B_r_s_1 / B_eps_L)
191           0.0979720961417486907   sqrt(B_EN)
192           0.0979158757099342081   cbrt(B_EN)
193           0.0978083179688063603   (A_EA * A_r_val)
194           0.0977369822216688777   (B_IP^3)
195           0.0977128241287103377   B_EN
196           0.0966050150842626409   (B_IP / A_eps_L)
197           0.0962282455862625841   (B_eps_L / A_eps_H)
198           0.0956075859090988883   (B_EN^2)
199           0.0954106947459836402   (B_IP^2)
#-----------------------------------------------------------------------
200           0.106136014562246056    (B_eps_L * A_cB)
201           0.0845635191743491776   (|B_EA - A_EA|)
202           0.0821975979637313281   (B_eps_L + A_EA)
203           0.0819338019702288561   (B_eps_L / A_IP)
204           0.0816448658542254652   (B_EA / A_cA)
205           0.0726263778247118325   (B_eps_L + A_EN)
206           0.0709774679475101977   (B_Z / A_cA)
207           0.0697477627438188308   (B_EA / A_r_s_1)
208           0.0693285200036956734   (B_eps_L / A_r_s_1)
209           0.0671474474558692164   (B_EA * A_EA)
210           0.0635661387763426927   (B_eps_L^2)
211           0.061609623907620599    (B_EA * A_r_val)
212           0.0602442339885400011   (B_eps_H * A_cAcB)
213           0.0601934166232411666   (B_eps_L^3)
214           0.0581295673916299854   (B_Z / A_cAcB)
215           0.0580107595690373609   (B_eps_H * A_cA)
216           0.0576046645852093461   (B_EA / A_cAcB)
217           0.0569342070271876971   (B_r_s - A_r_s_1)
218           0.0569342070271876971   B_eps_L
219           0.056003945491069955    (B_eps_L / B_EN)
220           0.0553089849193596633   (B_EN * B_eps_L)
221           0.0549449437062554322   (B_IP * A_cAcB)
222           0.0548993833235403572   (B_EA / A_EN)
223           0.0539328234423745237   (B_eps_H / A_cB)
224           0.053623630076318346    (B_EN - B_eps_L)
225           0.0532980332584182884   (B_eps_H * A_r_s_1)
226           0.0528662619842378279   (B_EA - B_eps_L)
227           0.0527364236366356584   (B_eps_L * B_r_s_1)
228           0.0526415757312092211   (B_eps_L * B_eps_H)
229           0.0524427300063110072   (B_eps_L + A_IP)
230           0.0519573217411962857   (B_eps_L + B_eps_H)
231           0.0515439235383343131   (B_IP * A_cA)
232           0.051184150379837437    (B_eps_H * A_Z)
233           0.0501787625656580846   (B_eps_L * B_r_s)
234           0.04945520078607199     (B_IP / A_EA)
235           0.0494340675201229846   (B_eps_L * A_r_s)
236           0.0487737829667846193   (B_EA + A_EA)
237           0.0487389603706779445   (B_EA * A_cB)
238           0.0485542321738595684   (A_r_s_1 / A_EA)
239           0.0484330730788917127   (B_IP / A_cB)
240           0.0484156227232690195   (B_IP * B_eps_L)
241           0.0480769863046341048   (B_eps_H / A_EA)
242           0.0475998317521186126   (A_eps_L / A_cA)
243           0.0472857651172194846   (A_IP / A_EA)
244           0.047165810773755236    (B_IP / A_cAcB)
245           0.0471061731573085007   (A_eps_L / A_cAcB)
246           0.0467743120141395768   (A_cA / A_EA)
247           0.0465159189437498732   (B_eps_L * A_r_val)
248           0.0463649034746979952   (A_eps_H / A_EA)
249           0.0463230631184515032   (B_IP * A_r_s_1)
#-----------------------------------------------------------------------
